import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class JDBCPrepareStatement {

    private static void insertRecord(int category_id, String name) {
        Connection connection = JDBCConnection.getJDBConnection("sakila");

        String sql = "insert into category (name) values (?)";

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            // preparedStatement.setInt(1, 17);
            preparedStatement.setString(1, name);

            int result = preparedStatement.executeUpdate();
            System.out.println("result = " + result);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    private static void deleteRecord(int category_id) {
        Connection connection = JDBCConnection.getJDBConnection("sakila");

        String sql = "delete from category where category_id=?";

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            // preparedStatement.setInt(1, 17);
            preparedStatement.setInt(1, category_id);

            int result = preparedStatement.executeUpdate();
            System.out.println("result = " + result);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }



    public static void main(String[] args) {
        insertRecord(19, "Comedy");
        // deleteRecord(31);
    }
}
