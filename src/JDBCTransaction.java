import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class JDBCTransaction {

    public static void main(String[] args) {
        Connection connection = JDBCConnection.getJDBConnection("new_schema");
        String sql1 = "update customer set balance=? where id=?";
        // String sql2 = "update customer set balance=? where id=?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql1);

            connection.setAutoCommit(false);

            preparedStatement.setDouble(1, 900);
            preparedStatement.setDouble(2, 1);
            preparedStatement.executeUpdate();

            preparedStatement.setDouble(1, 1400);
            preparedStatement.setDouble(2, 2);
            preparedStatement.executeUpdate();

            connection.commit();
            
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            System.out.println("Loi roi: " + e);
            try {
                connection.rollback();
                System.out.println("Rollback thanh cong!");
            } catch (SQLException e1) {
                // TODO Auto-generated catch block
                System.out.println(e);
            }
        }
    }


}
