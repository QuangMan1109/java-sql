import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JDBCStatement {

    private static void selectFromDB(Statement statement) throws SQLException {
        System.out.println("");
        System.out.println("------------Select Query");
        StringBuilder tableName = new StringBuilder("category");
        StringBuilder selectQuery = new StringBuilder("select * from " + tableName);
        StringBuilder columName = new StringBuilder("category_id");
        StringBuilder whereQuery = new StringBuilder(selectQuery + " where " + columName +"=14");

        ResultSet resultSet = statement.executeQuery(whereQuery.toString());

        while (resultSet.next()) {
            int id = resultSet.getInt("category_id");
            String name = resultSet.getString("name");

            System.out.println(id + " " + name);
        }
    }

    private static void createTable(Statement statement, String tableName) throws SQLException {
        System.out.println("");
        System.out.println("------------Create Query");
        StringBuilder createQuery = new StringBuilder("");
        createQuery.append("CREATE TABLE " + tableName + " (id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, firstname VARCHAR(30) NOT NULL)");

        int result = statement.executeUpdate(createQuery.toString());
        System.out.println("Result = " + result);

    }

    private static void dropTable(Statement statement, String tableName) throws SQLException {
        System.out.println("");
        System.out.println("------------Delete Query");
        StringBuilder createQuery = new StringBuilder("");
        createQuery.append("DROP TABLE " + tableName + ";");

        int dropResult = statement.executeUpdate(createQuery.toString());
        System.out.println("deleteResult = " + dropResult);

    }

    private static void insertRecord(Statement statement, String tableName) throws SQLException {
        System.out.println("");
        System.out.println("------------Insert Query");
        StringBuilder insertQuery = new StringBuilder("");
        insertQuery.append("INSERT INTO " + tableName + "(category_id, name) VALUES (17, 'Thriller');");

        int insertResult = statement.executeUpdate(insertQuery.toString());
        System.out.println("result = " + insertResult);
    }

    private static void setRecord(Statement statement, String tableName) throws SQLException {
        System.out.println("");
        System.out.println("------------Set Query");
        StringBuilder setQuery = new StringBuilder("");
        setQuery.append("UPDATE " + tableName + " SET category_id=18 WHERE category_id=20");

        int result = statement.executeUpdate(setQuery.toString());
        System.out.println("result = " + result);
    }

    private static void deleteRecord(Statement statement, String tableName) throws SQLException {
        System.out.println("");
        System.out.println("------------Delete Query");
        StringBuilder setQuery = new StringBuilder("");
        setQuery.append("DELETE FROM " + tableName + " WHERE category_id=17");

        int result = statement.executeUpdate(setQuery.toString());
        System.out.println("result = " + result);
    }

    public static void main(String[] args) {

        StringBuilder dbName = new StringBuilder("sakila");
        try {
            Connection connection = JDBCConnection.getJDBConnection(dbName.toString());
            Statement statement = connection.createStatement();

            selectFromDB(statement);
            // createTable(statement, "myguests");
            // dropTable(statement, "myguests");
            // insertRecord(statement, "category");
            // setRecord(statement, "category");
            deleteRecord(statement, "category");

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}

